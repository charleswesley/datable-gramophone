(function(Game, $, undefined) {
    Game.data = {
        loc: 0,
        total_loc: 0,
        workers: {},
        upgrades: {},
        date: new Date(2000, 0, 1),
        news: [],
        unread_news: []
    };

    Game.fps = 5;
    Game.mspf = 1000 / Game.fps;
    Game.bgsps = 1;

    Game.lookups = {};

    Game.state = {
        date_reviver: function(key, value) {
            var a;
            if (typeof value === 'string') {
                a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)(Z|([+\-])(\d{2}):(\d{2}))$/.exec(value);
                if (a) {
                    return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                                    +a[5], +a[6]));
                }
            }
            return value;
        },

        save: function () {
            $.cookie('save-data', JSON.stringify(Game.data));
        },

        load: function () {
            if ($.cookie('save-data')) {
                $.extend(true, Game.data, JSON.parse($.cookie('save-data'), Game.state.date_reviver));
            }
            // TODO: invalidate cache for everything here
        },

        new_game: function () {
            $.removeCookie('save-data');
            location.reload();
        }
    };

    Game.cache = {
        valid_rate: false,
        check_rate: function () {
            if (!Game.cache.valid_rate) {
                Game.cache.lpgs = Game.loc.compute_lpgs();
                Game.cache.lpc = Game.loc.compute_lpc();
                Game.cache.gsps = Game.loc.compute_gsps();
                Game.cache.gspf = Game.loc.compute_gspf();
                Game.cache.valid_rate = true;
            }
        },
        lpgs: 0,
        lpc: 1,
        gsps: Game.bgsps,
        gspf: Game.bgsps / Game.fps,
        ticker: [],
        workers: {}
    };

    Game.loc = {
        add: function (amount) {
            Game.data.loc += amount;
            Game.data.total_loc += amount;
            $(Game).trigger('change_loc');
        },

        subtract: function (amount) {
            Game.data.loc -= amount;
            $(Game).trigger('change_loc');
        },

        compute_lpgs: function () {
            var key,
                worker,
                lpgs = 0;
            for (key in Game.workers) {
                if (Game.workers.hasOwnProperty(key)) {
                    worker = Game.workers[key];
                    lpgs += worker.lpgs();
                }
            }
            return lpgs;
        },

        compute_lpc: function () {
            var lpc = 1;
            if (Game.data.upgrades.typing_lessons.count) {
                lpc *= 2;
            }
            return lpc;
        },

        compute_gsps: function () {
            var gsps = Game.bgsps;
            if (Game.data.upgrades.accelerate_time.count) {
                gsps *= 2;
            }
            return gsps;
        },

        compute_gspf: function () {
            return Game.loc.compute_gsps() / Game.fps;
        },

        accumulate: function () {
            this.add(Game.loc.lpgs() * Game.loc.gspf());
        },

        lpgs: function () {
            Game.cache.check_rate();
            return Game.cache.lpgs;
        },

        lpc: function () {
            Game.cache.check_rate();
            return Game.cache.lpc;
        },

        gsps: function () {
            Game.cache.check_rate();
            return Game.cache.gsps;
        },

        gspf: function () {
            Game.cache.check_rate();
            return Game.cache.gspf;
        }
    };

    Game.date = {
        format_date: function (date) {
            var months = [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
                hours = date.getHours(),
                minutes = date.getMinutes(),
                seconds = date.getSeconds(),
                ampm = 'a.m.';

            if (hours > 11) {
                ampm = 'p.m.';
            }
            if (hours > 12) {
                hours -= 12;
            }
            if (hours === 0) {
                hours = 12;
            }
            if (minutes < 10) {
                minutes = '0' + minutes;
            }
            if (seconds < 10) {
                seconds = '0' + seconds;
            }

            return months[date.getMonth()] + '. ' + date.getDate() + ', ' + date.getFullYear() + ' ' + hours + ':' + minutes + ':' + seconds + ' ' + ampm;
        },

        time: function () {
            return Game.date.format_date(Game.data.date);
        }
    };

    Game.advance_time = function () {
        Game.data.date.setMilliseconds(Game.data.date.getMilliseconds() + Game.loc.gspf() * 1000);
    };

    Game.redraw = function () {
        $('#lines_of_code').text(Math.floor(Game.data.loc));

        $('#clock').text(Game.date.time());
    };

    $('#write_code').on('click', function (e) {
        e.preventDefault();
        Game.loc.add(Game.loc.lpc());
    });

    Game.initialize = function () {
        var i,
            fixture;

        Game.workers = {};
        for (i = 0; i < Game.fixtures.workers.length; i += 1) {
            fixture = Game.fixtures.workers[i];
            Game.workers[fixture.name] = new Game.lib.Worker(
                Game.data.workers[fixture.name] = {},
                Game.cache.workers[fixture.name] = {},
                fixture.name,
                fixture.label,
                fixture.label_plural,
                fixture.description,
                fixture.bcost,
                fixture.blps,
                fixture.unlock_conditions,
                fixture.kwargs
            );
        }
        Game.upgrades = {};
        for (i = 0; i < Game.fixtures.upgrades.length; i += 1) {
            fixture = Game.fixtures.upgrades[i];
            Game.upgrades[fixture.name] = new Game.lib.Upgrade(
                Game.data.upgrades[fixture.name] = {},
                fixture.name,
                fixture.label,
                fixture.description,
                fixture.bcost,
                fixture.unlock_conditions,
                fixture.kwargs
            );
        }

        Game.state.load();

        Game.loop();

        Game.os1.initialize();
    };

    Game.loop = function () {
        Game.advance_time();
        Game.loc.accumulate();
        $(Game).trigger('unlock_stuff');
        Game.redraw();
        setTimeout(Game.loop, Game.mspf);
    };

    Game.initialize();
}(window.Game = window.Game || {}, jQuery));
