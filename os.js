(function(Game, $, undefined) {
    Game.os1 = Game.os1 || {};
    Game.os1.keys = {};
    Game.os1.prev_command = '';

    Game.os1.print = function (input) {
        $('#prompt').before(input + '<br /><br />');
    };

    Game.os1.evaluate = function (input) {
        var unlock_item;

        Game.os1.prev_command = input;
        input = input.toLowerCase().trim().replace(String.fromCharCode(160), ' ');
        if (input === 'write code') {
            Game.loc.add(Game.loc.lpc());
            Game.news.post('You have ' + Math.floor(Game.data.loc) + ' lines of code.');
        } else if (input.indexOf('buy ') === 0) {
            unlock_item = input.slice(4);
            try {
                Game.lookups.console[unlock_item].buy();
            } catch (err) {
                switch (err.name) {
                    case 'TypeError':
                    case 'NotUnlocked':
                        Game.news.post('Unknown command.');
                        break;
                    case 'AlreadyBought':
                        Game.news.post('You already bought that!');
                        break;
                    case 'NotAffordable':
                        Game.news.post('Insufficient LOC.');
                        break;
                }
            }
        } else {
            Game.news.post('Unknown command.');
        }
        $(Game).trigger('unlock_stuff');
        Game.os1.print(Game.news.read_unread().join('<br />'));
        $(window).scrollTop($(document).height());
    };

    Game.os1.new_post = function (e, news_obj) {
        Game.news.post_unread(news_obj);
    };

    Game.os1.initialize = function () {
        $('body').on('keydown', function (e) {
            var input, new_prompt;
            console.log(e.keyCode);
            if (17 in Game.os1.keys || 18 in Game.os1.keys || 91 in Game.os1.keys || 93 in Game.os1.keys) {
                return;
            }
            e.preventDefault();
            Game.os1.keys[e.keyCode] = true;
            if ((e.keyCode >= 65 && e.keyCode <= 90) || e.keyCode === 189) {
                character = String.fromCharCode(e.keyCode);
                if (e.keyCode === 189 && 16 in Game.os1.keys) {
                    character = '_';
                }
                $('#prompt .input').append(character);
            } else if (e.keyCode === 32) {
                $('#prompt .input').append('&nbsp;');
            } else if (e.keyCode === 8) {
                $('#prompt .input').text($('#prompt .input').text().slice(0, -1));
            } else if (e.keyCode === 13) {
                input = $('#prompt .input').text();
                new_prompt = $('#prompt').clone();
                $('#prompt').removeAttr('id').find('.cursor').remove();
                $('#os_1').append(new_prompt);
                new_prompt.find('.input').text('');
                Game.os1.evaluate(input);
            } else if (e.keyCode === 38 && Game.data.upgrades.command_history.count > 0) {
                $('#prompt .input').text(Game.os1.prev_command);
            }
        }).on('keyup', function (e) {
            delete Game.os1.keys[e.keyCode];
        });

        $(Game).on('new_post', Game.os1.new_post);
    };
}(window.Game = window.Game || {}, jQuery));
