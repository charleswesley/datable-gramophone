(function(Game, $, undefined) {
    Game.lib = Game.lib || {};
    Game.lib.Resource = function (data, name, label, description, bcost, unlock_conditions) {
        this.name = name;
        this.label = label;
        this.description = description;
        this.bcost = bcost;
        this.unlock_conditions = unlock_conditions;

        this.data = data;
        this.data.count = 0;
        this.data.unlocked = false;
        this.buyable = false;

        this.buy = function (e) {
            if (!this.buyable) {
                return;
            }

            Game.loc.subtract(this.cost());
            this.data.count += 1;

            $(this).trigger('bought');

            Game.cache.valid_rate = false;
        };

        this.check_unlock = function () {
            // console.log('checking lock status for ' + this.type + ' ' + this.label);
            var key;
            if (!this.data.unlocked) {
                for (key in this.unlock_conditions) {
                    if (this.unlock_conditions.hasOwnProperty(key)) {
                        if (!this.unlock_conditions[key]()) {
                            return;
                        }
                    }
                }
                this.data.unlocked = true;
            }
        };

        this.unlock = function (id, oldval, newval) {
            if (newval) {
                this.render();
                $(Game).off('unlock_stuff', $.proxy(this.check_unlock, this));
                $(Game).on('change_loc', $.proxy(this.check_buyable, this));
            }
            return newval;
        };

        this.check_buyable = function () {
            // console.log('Check buyable for ' + this.type + ' ' + this.label);
            if (this.buyable && Game.data.loc < this.cost()) {
                this.buyable = false;
                this.$el.addClass('disabled');
            } else if (!this.buyable && Game.data.loc >= this.cost()) {
                this.buyable = true;
                this.$el.removeClass('disabled');
            }
        };

        this.remove = function () {
            $(Game).off('change_loc', $.proxy(this.check_buyable, this));
            this.$el.remove();
        };

        this.initialize = function() {
            $(Game).on('unlock_stuff', $.proxy(this.check_unlock, this));
            this.data.watch('unlocked', $.proxy(this.unlock, this));
        };
        this.initialize();
    };

    Game.lib.Worker = function (data, cache, name, label, label_plural, description, bcost, blps, unlock_conditions) {
        Game.lib.Resource.apply(this, [data, name, label, description, bcost, unlock_conditions]);

        this.type = 'worker';
        this.label_plural = label_plural;
        this.blpgs = blps / Game.bgsps;

        this.cache = cache;
        this.cache.cost = this.bcost;
        this.cache.lpgs = this.blpgs;
        this.cache.valid_rate = false;

        this.cache.check_rate = function () {
            if (!this.cache.valid_rate) {
                this.cache.cost = this.compute_cost();
                this.cache.lpgs = this.compute_lpgs();
            }
            this.cache.valid_rate = true;
        };

        this.compute_cost = function () {
            return Math.round(Math.pow(1.05, this.data.count) * this.bcost);
        };

        this.compute_lpgs = function () {
            return this.data.count * this.blpgs;
        };

        this.cost = function () {
            $.proxy(this.cache.check_rate, this)();
            return this.cache.cost;
        };

        this.lpgs = function () {
            $.proxy(this.cache.check_rate, this)();
            return this.cache.lpgs;
        };

        this.render = function () {
            this.$el = $('<div id="worker_' + this.name + '" class="worker disabled">' +
                '<a href="#" class="button right">Hire</a>' +
                '<div class="title">' + this.label_plural + '</div>' +
                '<div class="count">' + this.data.count + '</div>' +
                '<div class="cost">' + this.cost() + '</div>' +
                '</div>').on('click', '.button', $.proxy(this.buy, this));
            $('#workers').append(this.$el);
        };

        this.bought = function () {
            this.cache.valid_rate = false;

            $('#' + this.type + '_' + this.name + ' .count').text(this.data.count);
            $('#' + this.type + '_' + this.name + ' .cost').text(this.cost());
        };
        $(this).on('bought', $.proxy(this.bought, this));
    };

    Game.lib.Upgrade = function (data, name, label, description, bcost, unlock_conditions) {
        Game.lib.Resource.apply(this, [data, name, label, description, bcost, unlock_conditions]);
        this.type = 'upgrade';

        this.cost = function () {
            return this.bcost;
        };

        this.render = function () {
            this.$el = $('<div id="upgrade_' + this.name + '" class="upgrade disabled">' +
                '<a href="#" class="button right">Buy</a>' +
                '<div class="title">' + this.label + '</div>' +
                '<div class="count">' + this.data.count + '</div>' +
                '<div class="cost">' + this.cost() + '</div>' +
                '</div>').on('click', '.button', $.proxy(this.buy, this));
            $('#upgrades').append(this.$el);
        };

        this.bought = function () {
            this.remove();
        };
        $(this).on('bought', $.proxy(this.bought, this));
    };

    Game.fixtures = Game.fixtures || {};
    Game.fixtures.workers = [
        {
            name: 'intern',
            label: 'Intern',
            label_plural: 'Interns',
            description: 'description for interns',
            bcost: 17,
            blps: 0.1,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 18); }
            }
        },
        {
            name: 'programmer',
            label: 'Programmer',
            label_plural: 'Programmers',
            description: 'description for programmers',
            bcost: 83,
            blps: 0.5,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 83); }
            }
        },
        {
            name: 'project_team',
            label: 'Project team',
            label_plural: 'Project teams',
            description: 'description for project teams',
            bcost: 1000,
            blps: 4,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 1500); }
            }
        },
        {
            name: 'dev_studio',
            label: 'Development studio',
            label_plural: 'Development studios',
            description: 'description for development studios',
            bcost: 10000,
            blps: 20,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 15000); }
            }
        },
        {
            name: 'outsourcing_contract',
            label: 'Outsourcing contract',
            label_plural: 'Outsourcing contracts',
            description: 'description for outsourcing contracts',
            bcost: 75000,
            blps: 50,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 3000000); }
            }
        }
    ];

    Game.fixtures.upgrades = [
        {
            name: 'typing_lessons',
            label: 'Typing Lessons',
            description: 'Kick things up a notch with Mavis Bacon. Doubles your L/s.',
            bcost: 100,
            unlock_conditions: {
                interns: function () { return (Game.data.workers.intern.count >= 5); }
            }
        },
        {
            name: 'accelerate_time',
            label: 'Accelerate Time',
            description: 'Time passes twice as fast.',
            bcost: 100,
            unlock_conditions: {
                interns: function () { return (Game.data.workers.intern.count >= 5); }
            }
        },
    ]
}(window.Game = window.Game || {}, jQuery));
