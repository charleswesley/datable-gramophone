(function(Game, $, undefined) {
    Game.fixtures = Game.fixtures || {};
    Game.fixtures.workers = [
        {
            name: 'intern',
            label: 'Intern',
            label_plural: 'Interns',
            description: 'description for interns',
            bcost: 17,
            blps: 0.1,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 18); }
            },
            kwargs: {}
        },
        {
            name: 'programmer',
            label: 'Programmer',
            label_plural: 'Programmers',
            description: 'description for programmers',
            bcost: 83,
            blps: 0.5,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 83); }
            },
            kwargs: {}
        },
        {
            name: 'project_team',
            label: 'Project team',
            label_plural: 'Project teams',
            description: 'description for project teams',
            bcost: 1000,
            blps: 4,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 1500); }
            },
            kwargs: {}
        },
        {
            name: 'dev_studio',
            label: 'Development studio',
            label_plural: 'Development studios',
            description: 'description for development studios',
            bcost: 10000,
            blps: 20,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 15000); }
            },
            kwargs: {}
        },
        {
            name: 'outsourcing_contract',
            label: 'Outsourcing contract',
            label_plural: 'Outsourcing contracts',
            description: 'description for outsourcing contracts',
            bcost: 75000,
            blps: 50,
            unlock_conditions: {
                loc: function () { return (Game.data.total_loc >= 3000000); }
            },
            kwargs: {}
        }
    ];

    Game.fixtures.upgrades = [
        {
            name: 'command_history',
            label: 'Command History',
            description: 'Use the up arrow to reload your previous command.',
            bcost: 3,
            unlock_conditions: {
                loc: function () { return (Game.data.loc >= 3); }
            },
            kwargs: {}
        },
        {
            name: 'typing_lessons',
            label: 'Typing Lessons',
            description: 'Kick things up a notch with Mavis Bacon. Doubles your L/click.',
            bcost: 100,
            unlock_conditions: {
                interns: function () { return (Game.data.workers.intern.count >= 5); }
            },
            kwargs: {}
        },
        {
            name: 'accelerate_time',
            label: 'Accelerate Time',
            description: 'Time passes twice as fast.',
            bcost: 100,
            unlock_conditions: {
                interns: function () { return (Game.data.workers.intern.count >= 5); }
            },
            kwargs: {}
        }
    ];
}(window.Game = window.Game || {}, jQuery));
