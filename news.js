(function(Game, $, undefined) {
    Game.news = {
        post: function(news_obj) {
            Game.data.news.push(news_obj);
            $(Game).trigger('new_post', news_obj);
        },

        post_unread: function(news_obj) {
            Game.data.unread_news.push(news_obj);
        },

        read_unread: function() {
            var unread = Game.data.unread_news;
            Game.data.unread_news = [];
            return unread;
        }
    };
}(window.Game = window.Game || {}, jQuery));
