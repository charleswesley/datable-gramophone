(function(Game, $, undefined) {
    Game.lib = Game.lib || {};
    Game.lib.Resource = function (data, name, label, description, bcost, unlock_conditions, kwargs) {
        this.name = name;
        this.label = label;
        this.description = description;
        this.bcost = bcost;
        this.unlock_conditions = unlock_conditions;
        if (kwargs.hasOwnProperty('console_label')) {
            this.console_label = kwargs.console_label;
        } else {
            this.console_label = this.label.toLowerCase().replace(' ', '_').replace('-', '');
        }
        Game.lookups.console = Game.lookups.console || {};
        Game.lookups.console[this.console_label] = this;

        this.data = data;
        this.data.count = 0;
        this.data.unlocked = false;
        this.affordable = false;

        this.buy = function (e) {
            this.can_buy();

            Game.loc.subtract(this.cost());
            this.data.count += 1;

            Game.news.post('PURCHASED: ' + this.label + '<br />' + this.description);

            $(this).trigger('bought');

            Game.cache.valid_rate = false;
        };

        this.check_unlock = function () {
            // console.log('checking lock status for ' + this.type + ' ' + this.label);
            var key;
            if (!this.data.unlocked) {
                for (key in this.unlock_conditions) {
                    if (this.unlock_conditions.hasOwnProperty(key)) {
                        if (!this.unlock_conditions[key]()) {
                            return;
                        }
                    }
                }
                this.data.unlocked = true;
            }
        };

        this.unlock = function (id, oldval, newval) {
            if (newval) {
                Game.news.post(this.type.toUpperCase() + ' AVAILABLE: ' + this.label + '<br />' + 'COST: ' + this.cost() + ' LOC<br />Type "buy ' + this.console_label + '" to buy ' + this.type + '.');
                this.render();
                $(Game).off('unlock_stuff', $.proxy(this.check_unlock, this));
                $(Game).on('change_loc', $.proxy(this.check_affordable, this));
            }
            return newval;
        };

        this.check_affordable = function () {
            // console.log('Check affordable for ' + this.type + ' ' + this.label);
            if (this.affordable && Game.data.loc < this.cost()) {
                this.affordable = false;
                this.$el.addClass('disabled');
            } else if (!this.affordable && Game.data.loc >= this.cost()) {
                this.affordable = true;
                this.$el.removeClass('disabled');
            }
        };

        this.remove = function () {
            $(Game).off('change_loc', $.proxy(this.check_affordable, this));
            this.$el.remove();
        };

        this.initialize = function() {
            $(Game).on('unlock_stuff', $.proxy(this.check_unlock, this));
            this.data.watch('unlocked', $.proxy(this.unlock, this));
        };
        this.initialize();
    };

    Game.lib.Worker = function (data, cache, name, label, label_plural, description, bcost, blps, unlock_conditions, kwargs) {
        Game.lib.Resource.apply(this, [data, name, label, description, bcost, unlock_conditions, kwargs]);

        this.type = 'worker';
        this.label_plural = label_plural;
        this.blpgs = blps / Game.bgsps;

        this.cache = cache;
        this.cache.cost = this.bcost;
        this.cache.lpgs = this.blpgs;
        this.cache.valid_rate = false;

        this.cache.check_rate = function () {
            if (!this.cache.valid_rate) {
                this.cache.cost = this.compute_cost();
                this.cache.lpgs = this.compute_lpgs();
            }
            this.cache.valid_rate = true;
        };

        this.compute_cost = function () {
            return Math.round(Math.pow(1.05, this.data.count) * this.bcost);
        };

        this.compute_lpgs = function () {
            return this.data.count * this.blpgs;
        };

        this.cost = function () {
            $.proxy(this.cache.check_rate, this)();
            return this.cache.cost;
        };

        this.lpgs = function () {
            $.proxy(this.cache.check_rate, this)();
            return this.cache.lpgs;
        };

        this.render = function () {
            this.$el = $('<div id="worker_' + this.name + '" class="worker disabled">' +
                '<a href="#" class="button right">Hire</a>' +
                '<div class="title">' + this.label_plural + '</div>' +
                '<div class="count">' + this.data.count + '</div>' +
                '<div class="cost">' + this.cost() + '</div>' +
                '</div>').on('click', '.button', $.proxy(this.buy, this));
            $('#workers').append(this.$el);
        };

        this.can_buy = function () {
            if (!this.data.unlocked) {
                throw {name: 'NotUnlocked'};
            }
            if (!this.affordable || Game.data.loc < this.cost()) {
                throw {name: 'NotAffordable'};
            }
        };

        this.bought = function () {
            this.cache.valid_rate = false;

            $('#' + this.type + '_' + this.name + ' .count').text(this.data.count);
            $('#' + this.type + '_' + this.name + ' .cost').text(this.cost());
        };
        $(this).on('bought', $.proxy(this.bought, this));
    };

    Game.lib.Upgrade = function (data, name, label, description, bcost, unlock_conditions, kwargs) {
        Game.lib.Resource.apply(this, [data, name, label, description, bcost, unlock_conditions, kwargs]);
        this.type = 'upgrade';

        this.cost = function () {
            return this.bcost;
        };

        this.render = function () {
            this.$el = $('<div id="upgrade_' + this.name + '" class="upgrade disabled">' +
                '<a href="#" class="button right">Buy</a>' +
                '<div class="title">' + this.label + '</div>' +
                '<div class="count">' + this.data.count + '</div>' +
                '<div class="cost">' + this.cost() + '</div>' +
                '</div>').on('click', '.button', $.proxy(this.buy, this));
            $('#upgrades').append(this.$el);
        };

        this.can_buy = function () {
            if (!this.data.unlocked) {
                throw {name: 'NotUnlocked'};
            }
            if (this.data.count > 0) {
                throw {name: 'AlreadyBought'};
            }
            if (!this.affordable || Game.data.loc < this.cost()) {
                throw {name: 'NotAffordable'};
            }
        };

        this.bought = function () {
            this.remove();
        };
        $(this).on('bought', $.proxy(this.bought, this));
    };
}(window.Game = window.Game || {}, jQuery));
